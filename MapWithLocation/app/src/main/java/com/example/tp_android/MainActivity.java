package com.example.tp_android;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class MainActivity extends AppCompatActivity {

    private TextView dateTimeTextView;
    private TextView locationTextView;


    private final int REFRESH_RATE = 1000; // en millisecondes
    private Handler mHandler = new Handler();

    private Runnable mRefreshTimeTask = new Runnable() {
        @Override
        public void run() {

            int gmtOffset = +1;
            String gmtString = "";

            if (gmtOffset >= 0) {
                gmtString = "+" + gmtOffset;
            } else {
                gmtString = "" + gmtOffset;
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss 'GMT'X:00 yyyy", Locale.ENGLISH);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT" + gmtString));
            Date currentDateTime = Calendar.getInstance().getTime();

            dateTimeTextView.setText("Date : " + dateFormat.format(currentDateTime));

            mHandler.postDelayed(mRefreshTimeTask, REFRESH_RATE);

        }
    };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dateTimeTextView = (TextView) findViewById(R.id.textview1);
        locationTextView = (TextView) findViewById(R.id.textview2);

        // Obtenir la position actuelle
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                String[] perm = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};

                requestPermissions(perm,1);
            }
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                DecimalFormat decimalFormat = new DecimalFormat("#.000000");
                locationTextView.setText("Latitude: " + location.getLatitude() + "\nLongitude: " + location.getLongitude() + "\nPrécision (mètres) : " + decimalFormat.format(location.getAccuracy()));
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        });

        mHandler.post(mRefreshTimeTask);
    }
}
